package com.fellafeeds.sdk;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

@SuppressWarnings("unused")
public class FeedbackPreferences {

    private Context context;

    public FeedbackPreferences(Context context) {
        this.context = context;
    }

    private SharedPreferences getMasterPreferences() {
        return context.getSharedPreferences(context.getString(R.string.package_name), MODE_PRIVATE);
    }

    public void setAuthToken(String token) {
        getMasterPreferences().edit().putString(context.getString(R.string.auth_token), token).apply();
    }

    public String getAuthToken() {
        return getMasterPreferences().getString(context.getString(R.string.auth_token), "n/a");
    }

    public void setThankYouMessage(String message) {
        getMasterPreferences().edit().putString(context.getString(R.string.thank_you_key), message).apply();
    }

    public String getThankYouMessage() {
        return getMasterPreferences().getString(context.getString(R.string.thank_you_key), context.getString(R.string.thank_you_message));
    }

    public void setCtaMessage(String message) {
        getMasterPreferences().edit().putString(context.getString(R.string.cta_key), message).apply();
    }

    public String getCtaMessage() {
        return getMasterPreferences().getString(context.getString(R.string.cta_key), context.getString(R.string.cta_message));
    }

    public void setReferralMessage(String message) {
        getMasterPreferences().edit().putString(context.getString(R.string.referral_message_key), message).apply();
    }

    public String getReferralMessage() {
        return getMasterPreferences().getString(context.getString(R.string.referral_message_key), context.getString(R.string.referral_message));
    }

    public void setSuggestionMessage(String message) {
        getMasterPreferences().edit().putString(context.getString(R.string.suggestion_message_key), message).apply();
    }

    public String getSuggestionMessage() {
        return getMasterPreferences().getString(context.getString(R.string.suggestion_message_key), context.getString(R.string.suggestion_message));
    }

    public void setNpsMessage(String message) {
        getMasterPreferences().edit().putString(context.getString(R.string.nps_message_key), message).apply();
    }

    public String getNpsMessage() {
        return getMasterPreferences().getString(context.getString(R.string.nps_message_key), context.getString(R.string.nps_message));
    }

    public void setReferralFeature(boolean b) {
        getMasterPreferences().edit().putBoolean(context.getString(R.string.referral_key), b).apply();
    }

    public boolean isReferralFeature() {
        return getMasterPreferences().getBoolean(context.getString(R.string.referral_key), false);
    }

    public void setSkipSuggestion(boolean b) {
        getMasterPreferences().edit().putBoolean(context.getString(R.string.skip_suggestion_key), b).apply();
    }

    public boolean isSkipSuggestion() {
        return getMasterPreferences().getBoolean(context.getString(R.string.skip_suggestion_key), false);
    }

    public void setSkipNPS(boolean b) {
        getMasterPreferences().edit().putBoolean(context.getString(R.string.skip_nps_key), b).apply();
    }

    public boolean isSkipNPS() {
        return getMasterPreferences().getBoolean(context.getString(R.string.skip_nps_key), false);
    }

    /*public void setAskDOA(boolean b){
        getMasterPreferences().edit().putBoolean(context.getString(R.string.ask_doa_key),b).apply();
    }

    public boolean isAskDOA(){
       return getMasterPreferences().getBoolean(context.getString(R.string.ask_doa_key),true);
    }

    public void setAskDOB(boolean b){
        getMasterPreferences().edit().putBoolean(context.getString(R.string.ask_dob_key),b).apply();
    }

    public boolean isAskDOB(){
        return getMasterPreferences().getBoolean(context.getString(R.string.ask_dob_key),true);
    }*/

    public void setRateOne(Integer integer) {
        getMasterPreferences().edit().putInt(context.getString(R.string.rate_one_key), integer).apply();
    }

    public Integer getRateOne() {
        return getMasterPreferences().getInt(context.getString(R.string.rate_one_key), R.drawable.ic_rate_one);
    }

    public void setRateTwo(Integer rateTwo) {
        getMasterPreferences().edit().putInt(context.getString(R.string.rate_two_key), rateTwo).apply();
    }

    public Integer getRateTwo() {
        return getMasterPreferences().getInt(context.getString(R.string.rate_two_key), R.drawable.ic_rate_two);
    }

    public void setRateThree(Integer rateThree) {
        getMasterPreferences().edit().putInt(context.getString(R.string.rate_three_key), rateThree).apply();
    }

    public Integer getRateThree() {
        return getMasterPreferences().getInt(context.getString(R.string.rate_three_key), R.drawable.ic_rate_three);
    }

    public void setRateFour(Integer rateFour) {
        getMasterPreferences().edit().putInt(context.getString(R.string.rate_four_key), rateFour).apply();
    }

    public Integer getRateFour() {
        return getMasterPreferences().getInt(context.getString(R.string.rate_four_key), R.drawable.ic_rate_four);
    }

    public void setRateFive(Integer rateFive) {
        getMasterPreferences().edit().putInt(context.getString(R.string.rate_five_key), rateFive).apply();
    }

    public Integer getRateFive() {
        return getMasterPreferences().getInt(context.getString(R.string.rate_five_key), R.drawable.ic_rate_five);
    }

    public void setThankYou(Integer thankYou) {
        getMasterPreferences().edit().putInt(context.getString(R.string.thank_image_key), thankYou).apply();
    }

    public Integer getThankYou() {
        return getMasterPreferences().getInt(context.getString(R.string.thank_image_key), R.drawable.ic_thank_you);
    }

}
