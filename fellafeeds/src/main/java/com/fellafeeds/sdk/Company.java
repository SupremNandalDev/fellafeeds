/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

 class Company {

    @SerializedName("sector")
    @Expose
    private Sector sector;
    @SerializedName("name")
    @Expose
    private String name;

    public Sector getSector() {
        return sector;
    }

    @SuppressWarnings("unused")
    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
