/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = FeedbackOptions.class, version = 1)
abstract class FeedbackOptionDB extends RoomDatabase {

    private static final String DATABASE_NAME = "feedback_options";
    private static FeedbackOptionDB instance;

    static FeedbackOptionDB getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, FeedbackOptionDB.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    abstract FeedbackOptionDao feedbackOptionDao();
}