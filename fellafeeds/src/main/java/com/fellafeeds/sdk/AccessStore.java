/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

class AccessStore {

    @SerializedName("feedback_takers_data")
    @Expose
    private List<FeedbackTakersDatum> feedbackTakersData = null;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("service")
    @Expose
    private List<Service> service = null;
    @SerializedName("feedback_forms")
    @Expose
    private List<FeedbackForm_> feedbackForms = null;
    @SerializedName("feedback_takers")
    @Expose
    private List<String> feedbackTakers = null;
    @SerializedName("id")
    @Expose
    private Integer id;

    public List<FeedbackTakersDatum> getFeedbackTakersData() {
        return feedbackTakersData;
    }

    @SuppressWarnings("unused")
    public void setFeedbackTakersData(List<FeedbackTakersDatum> feedbackTakersData) {
        this.feedbackTakersData = feedbackTakersData;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Service> getService() {
        return service;
    }

    public void setService(List<Service> service) {
        this.service = service;
    }

    public List<FeedbackForm_> getFeedbackForms() {
        return feedbackForms;
    }

    @SuppressWarnings("unused")
    public void setFeedbackForms(List<FeedbackForm_> feedbackForms) {
        this.feedbackForms = feedbackForms;
    }

    @SuppressWarnings("unused")
    public List<String> getFeedbackTakers() {
        return feedbackTakers;
    }

    @SuppressWarnings("unused")
    public void setFeedbackTakers(List<String> feedbackTakers) {
        this.feedbackTakers = feedbackTakers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
