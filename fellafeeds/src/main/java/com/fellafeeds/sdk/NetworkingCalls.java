package com.fellafeeds.sdk;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class NetworkingCalls {

    private Context context;
    private String authToken;
    private NetworkingCallbacks callbacks;
    private RequestQueue requestQueue;
    private FeedbackPreferences preferences;
    private static final String TAG = "NetworkingCalls";

    public NetworkingCalls(Context context, NetworkingCallbacks callbacks) {
        this.context = context;
        this.callbacks = callbacks;
        preferences = new FeedbackPreferences(context);
        authToken = preferences.getAuthToken();
        requestQueue = Volley.newRequestQueue(context);
    }

    public void getFeedbackForms() {
        StringRequest request = new StringRequest(
                Request.Method.GET,
                "https://fellafeeds.com/feedbacks/api/feedback-forms/new/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            FeedbackFormDao feedbackFormDao = FeedbackFormDB.getInstance(context).feedbackFormDao();
                            FeedbackQuestionDao feedbackQuestionDao = FeedbackQuestionDB.getInstance(context).feedbackQuestionDao();
                            FeedbackOptionDao feedbackOptionDao = FeedbackOptionDB.getInstance(context).feedbackOptionDao();
                            EmployeeDataDao employeeDataDao = EmployeeDataDB.getInstance(context).employeeDataDao();

                            try {
                                employeeDataDao.removeEmployeesData();
                                feedbackFormDao.removeFeedbackForm();
                                feedbackQuestionDao.removeFeedbackQuestions();
                                feedbackOptionDao.removeFeedbackOptions();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            FeedbackDatum[] data = gson.fromJson(response, FeedbackDatum[].class);
                            FeedbackDatum feedback = data[0];

                            preferences.setReferralFeature(feedback.getReferralFeature());

                            List<FeedbackTakersDatum> feedbackTakersDatumList = feedback.getAccessStore().get(0).getFeedbackTakersData();
                            for (int b = 0; b < feedbackTakersDatumList.size(); b++) {
                                EmployeesData employeesData = new EmployeesData();
                                employeesData.setEmployee_id(feedbackTakersDatumList.get(b).getId());
                                employeesData.setName(feedbackTakersDatumList.get(b).getName());
                                employeeDataDao.insertEmployeeData(employeesData);
                            }

                            List<FeedbackForm_> list = feedback.getAccessStore().get(0).getFeedbackForms();
                            for (FeedbackForm_ form_ : list) {
                                FeedbackForm temp = new FeedbackForm();
                                temp.setPk(form_.getPk());
                                temp.setTitle(form_.getTitle());
                                feedbackFormDao.insertFeedbackForm(temp);
                                for (int y = 0; y < form_.getQuestions().size(); y++) {
                                    Question question = form_.getQuestions().get(y);
                                    FeedbackQuestions tempQues = new FeedbackQuestions();
                                    tempQues.setType(question.getType());
                                    tempQues.setMapid(Integer.parseInt(form_.getPk()));
                                    tempQues.setPk(question.getPk());
                                    tempQues.setTitle(question.getTitle());
                                    feedbackQuestionDao.insertFeedbackQuestion(tempQues);
                                    for (int a = 0; a < question.getOptions().size(); a++) {
                                        Option option = question.getOptions().get(a);
                                        FeedbackOptions feedbackOptions = new FeedbackOptions();
                                        feedbackOptions.setMapid(question.getPk());
                                        feedbackOptions.setPk(option.getPk());
                                        feedbackOptions.setSatisfaction_score(option.getSs());
                                        feedbackOptions.setTitle(option.getTitle());
                                        feedbackOptionDao.insertFeedbackQuestion(feedbackOptions);
                                    }
                                }
                            }
                            callbacks.onSuccess();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "onResponse: ", e);
                            NetworkingCallbacks callbacks;
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "onErrorResponse: ", error);
                callbacks.onFailed();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Token " + authToken);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                (int) TimeUnit.SECONDS.toMillis(5),
                1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    public void uploadFeedback(UploadingFeedback uploadingFeedback) {
        JsonObjectRequest request = new JsonObjectRequest("https://fellafeeds.com/feedbacks/api/feedback/", makeJsonObject(uploadingFeedback), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callbacks.onSuccess();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "onErrorResponse: ", error);
                callbacks.onFailed();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Token " + authToken);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(3000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    private JSONObject makeJsonObject(UploadingFeedback uploadingFeedback) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("source", "API");
            jsonObj.put("is_task", false);
            jsonObj.put("is_task_verified", false);
            jsonObj.put("otp_sent", uploadingFeedback.isOtpSent());
            jsonObj.put("otp_verified", uploadingFeedback.isOtpVerified());
            jsonObj.put("start_time", uploadingFeedback.getFeedbackStart());
            jsonObj.put("nps", uploadingFeedback.getNps()); //int
            jsonObj.put("referto", uploadingFeedback.getReferto()); //string
            jsonObj.put("referto_email", uploadingFeedback.getReferto_email());
            JSONObject refer = new JSONObject();
            refer.put("mobile", uploadingFeedback.getReferto());
            refer.put("email", uploadingFeedback.getReferto_email());
            refer.put("name", " ");
            jsonObj.put("refer", refer);
            jsonObj.put("comment", uploadingFeedback.getComment()); //string
            jsonObj.put("timestamp", uploadingFeedback.getTimestamp()); //string
            jsonObj.put("bill", uploadingFeedback.getBill()); //string
            jsonObj.put("amount", uploadingFeedback.getAmount()); //string
            jsonObj.put("table", uploadingFeedback.getTable()); //string
            jsonObj.put("remarks", uploadingFeedback.getRemarks()); //string
            jsonObj.put("feedback_form", uploadingFeedback.getFeedback_form()); //int
            JSONObject jsonFeedbackTaker = new JSONObject();
            jsonFeedbackTaker.put("pin", "1111"); //string
            jsonObj.put("feedback_taker", jsonFeedbackTaker);
            JSONObject jsonFeedbacker = new JSONObject();
            jsonFeedbacker.put("first_name", uploadingFeedback.getUserFirstName()); //string
            jsonFeedbacker.put("last_name", uploadingFeedback.getUserLastName()); //string
            jsonFeedbacker.put("mobile", uploadingFeedback.getUserMobile()); //string
            jsonFeedbacker.put("email", uploadingFeedback.getUserEmail()); //string
            jsonFeedbacker.put("dob", uploadingFeedback.getUserDob()); //string  uploadingFeedback.getFeedbacker().getDob()
            jsonFeedbacker.put("anni", uploadingFeedback.getUserAnni()); //string uploadingFeedback.getFeedbacker().getAnni()
            jsonObj.put("feedbacker", jsonFeedbacker);
            JSONArray jsonAnswersArray = new JSONArray();
            List<Answers> answers = uploadingFeedback.getAnswers();
            for (int j = 0; j < answers.size(); j++) {
                JSONObject answerObj = new JSONObject();
                answerObj.put("question", answers.get(j).getQuestion()); //string

                List<String> myList = new ArrayList<>(Arrays.asList(answers.get(j).getOption().split(",")));
                JSONArray ansOptionsArray = new JSONArray();

                for (String a : myList)
                    if (!a.isEmpty())
                        ansOptionsArray.put(Integer.parseInt(a));

                answerObj.put("options", ansOptionsArray);
                answerObj.put("text", answers.get(j).getText());
                jsonAnswersArray.put(answerObj);
            }
            jsonObj.put("answers", jsonAnswersArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

}
