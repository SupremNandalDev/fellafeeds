package com.fellafeeds.sdk;

public interface NetworkingCallbacks {

    void onComplete();

    void onSuccess();

    void onFailed();

}
