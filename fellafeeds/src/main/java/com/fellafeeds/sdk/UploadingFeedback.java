package com.fellafeeds.sdk;

import java.util.List;

class UploadingFeedback {

    private String comment;
    private String referto;
    private List<Answers> answers;
    private long timestamp;
    private int nps = 8;
    private int feedback_form;
    private String tag;
    private long feedbackStart;
    private boolean isOtpSent;
    private boolean isOtpVerified;
    private boolean isTask;
    private boolean isTaskVerified;
    private String referto_email;

    private String userMobile;
    private String userFirstName;
    private String userLastName;
    private String userEmail;
    private String userDob;
    private String userAnni;
    private String bill;
    private String amount;
    private String remarks;
    private String table;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getReferto() {
        return referto;
    }

    public void setReferto(String referto) {
        this.referto = referto;
    }

    public List<Answers> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answers> answers) {
        this.answers = answers;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getNps() {
        return nps;
    }

    public void setNps(int nps) {
        this.nps = nps;
    }

    public int getFeedback_form() {
        return feedback_form;
    }

    public void setFeedback_form(int feedback_form) {
        this.feedback_form = feedback_form;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public long getFeedbackStart() {
        return feedbackStart;
    }

    public void setFeedbackStart(long feedbackStart) {
        this.feedbackStart = feedbackStart;
    }

    public boolean isOtpSent() {
        return isOtpSent;
    }

    public void setOtpSent(boolean otpSent) {
        isOtpSent = otpSent;
    }

    public boolean isOtpVerified() {
        return isOtpVerified;
    }

    public void setOtpVerified(boolean otpVerified) {
        isOtpVerified = otpVerified;
    }

    public boolean isTask() {
        return isTask;
    }

    public void setTask(boolean task) {
        isTask = task;
    }

    public boolean isTaskVerified() {
        return isTaskVerified;
    }

    public void setTaskVerified(boolean taskVerified) {
        isTaskVerified = taskVerified;
    }

    public String getReferto_email() {
        return referto_email;
    }

    public void setReferto_email(String referto_email) {
        this.referto_email = referto_email;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserDob() {
        return userDob;
    }

    public void setUserDob(String userDob) {
        this.userDob = userDob;
    }

    public String getUserAnni() {
        return userAnni;
    }

    public void setUserAnni(String userAnni) {
        this.userAnni = userAnni;
    }

    public String getBill() {
        return bill;
    }

    public void setBill(String bill) {
        this.bill = bill;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }
}
