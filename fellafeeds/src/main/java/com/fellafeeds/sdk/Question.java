/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
class Question {

    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("detail")
    @Expose
    private Boolean detail;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("options")
    @Expose
    private List<Option> options = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("compulsary_question")
    @Expose
    private Boolean compulsary_question;
    @SerializedName("pre_sales")
    @Expose
    private Boolean pre_sales;

    public Boolean getPre_sales() {
        return pre_sales;
    }

    public void setPre_sales(Boolean pre_sales) {
        this.pre_sales = pre_sales;
    }

    public Boolean getCompulsary_question() {
        return compulsary_question;
    }

    public void setCompulsary_question(Boolean compulsary_question) {
        this.compulsary_question = compulsary_question;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public Boolean getDetail() {
        return detail;
    }

    public void setDetail(Boolean detail) {
        this.detail = detail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
