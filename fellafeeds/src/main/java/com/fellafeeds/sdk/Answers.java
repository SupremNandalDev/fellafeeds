/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

 class Answers {

    private int question;
    private String option;
    private String text;

    Answers(int question, String option, String text) {
        this.question = question;
        this.option = option;
        this.text = text;
    }

     String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

     int getQuestion() {
        return question;
    }

    public void setQuestion(int question) {
        this.question = question;
    }

     String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

}
