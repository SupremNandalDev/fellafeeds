/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "feedback_options")
class FeedbackOptions {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int pk;
    private int mapid;
    private String title;
    private int satisfaction_score;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public int getMapid() {
        return mapid;
    }

    public void setMapid(int mapid) {
        this.mapid = mapid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSatisfaction_score() {
        return satisfaction_score;
    }

    public void setSatisfaction_score(int satisfaction_score) {
        this.satisfaction_score = satisfaction_score;
    }
}
