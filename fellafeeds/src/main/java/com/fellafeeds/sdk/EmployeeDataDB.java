/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = EmployeesData.class, version = 1)
abstract class EmployeeDataDB extends RoomDatabase {

    private static final String DATABASE_NAME = "employees_data";
    private static EmployeeDataDB instance;

     static EmployeeDataDB getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, EmployeeDataDB.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    abstract EmployeeDataDao employeeDataDao();

}
