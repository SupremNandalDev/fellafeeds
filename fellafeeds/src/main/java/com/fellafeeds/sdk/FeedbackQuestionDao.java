/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
interface FeedbackQuestionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFeedbackQuestion(FeedbackQuestions feedbackQuestions);

    @Query("DELETE FROM feedback_question")
    void removeFeedbackQuestions();

    @Query("SELECT * FROM feedback_question WHERE mapid =:mapid ORDER BY id ASC")
    List<FeedbackQuestions> getQuestionByMapid(int mapid);

    @Query("SELECT * FROM feedback_question Where pk =:pk")
    FeedbackQuestions getQuestionByPk(int pk);

    @Query("SELECT COUNT(*) FROM feedback_question")
    int getFeedbackQuestionSize();

}
