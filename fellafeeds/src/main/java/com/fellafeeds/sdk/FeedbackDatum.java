/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
class FeedbackDatum {

    @SerializedName("referral_feature")
    @Expose
    private Boolean referralFeature;
    @SerializedName("company")
    @Expose
    private Company company;
    @SerializedName("access_store")
    @Expose
    private List<AccessStore> accessStore = null;
    @SerializedName("tags_available")
    @Expose
    private List<String> tagsAvailable = null;
    @SerializedName("visitors_feature")
    @Expose
    private Boolean visitorsFeature;
    @SerializedName("verification_feature")
    @Expose
    private Boolean optFeature;
    @SerializedName("feedback_flash")
    @Expose
    private boolean feedback_flash;

    public Boolean getReferralFeature() {
        return referralFeature;
    }

    public Company getCompany() {
        return company;
    }

    public List<AccessStore> getAccessStore() {
        return accessStore;
    }

    public List<String> getTagsAvailable() {
        return tagsAvailable;
    }

    public Boolean getVisitorsFeature() {
        return visitorsFeature;
    }

    public Boolean getOptFeature() {
        return optFeature;
    }

    public boolean isFeedback_flash() {
        return feedback_flash;
    }
}
