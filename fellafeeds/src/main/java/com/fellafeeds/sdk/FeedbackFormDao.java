/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
interface FeedbackFormDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFeedbackForm(FeedbackForm feedbackForms);

    @Query("DELETE FROM feedback_form")
    void removeFeedbackForm();

    @Query("SELECT * FROM feedback_form LIMIT 1")
    FeedbackForm getFeedbackForm();

}
