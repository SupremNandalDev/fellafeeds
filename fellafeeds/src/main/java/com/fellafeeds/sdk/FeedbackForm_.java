/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
class FeedbackForm_ {

    @SerializedName("pk")
    @Expose
    private String pk;
    @SerializedName("questions")
    @Expose
    private List<Question> questions = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("open_feedback_mode")
    @Expose
    private boolean open_feedback_mode;
    @SerializedName("survey_form")
    @Expose
    private boolean surveyForm;

    public boolean isSurveyForm() {
        return surveyForm;
    }

    public void setSurveyForm(boolean surveyForm) {
        this.surveyForm = surveyForm;
    }

    public boolean isOpen_feedback_mode() {
        return open_feedback_mode;
    }

    public void setOpen_feedback_mode(boolean open_feedback_mode) {
        this.open_feedback_mode = open_feedback_mode;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
