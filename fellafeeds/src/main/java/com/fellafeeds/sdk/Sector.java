/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
class Sector {

    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("multiple_employee")
    @Expose
    private Boolean multipleEmployee;
    @SerializedName("name")
    @Expose
    private String name;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getMultipleEmployee() {
        return multipleEmployee;
    }

    public void setMultipleEmployee(Boolean multipleEmployee) {
        this.multipleEmployee = multipleEmployee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
