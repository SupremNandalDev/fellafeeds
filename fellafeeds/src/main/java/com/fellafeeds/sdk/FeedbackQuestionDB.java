/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = FeedbackQuestions.class, version = 4)
abstract class FeedbackQuestionDB extends RoomDatabase {

    private static final String DATABASE_NAME = "feedback_question";
    private static FeedbackQuestionDB instance;

    static FeedbackQuestionDB getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, FeedbackQuestionDB.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    abstract FeedbackQuestionDao feedbackQuestionDao();

}
