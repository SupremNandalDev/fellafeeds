/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = FeedbackForm.class, version = 1)
abstract class FeedbackFormDB extends RoomDatabase {

    private static final String DATABASE_NAME = "feedback_form";
    private static FeedbackFormDB instance;

    static FeedbackFormDB getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, FeedbackFormDB.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    abstract FeedbackFormDao feedbackFormDao();
}
