/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;


@Dao
 interface EmployeeDataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertEmployeeData(EmployeesData employeesData);

    @Query("DELETE FROM employees_data")
    void removeEmployeesData();

    @Query("SELECT * FROM employees_data")
    List<EmployeesData> getAllEmployees();

    @Query("SELECT COUNT(*) FROM employees_data")
    int getEmployeeSize();


}
