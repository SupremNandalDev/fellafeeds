package com.fellafeeds.sdk;

import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class FeedbackFragment extends Fragment implements View.OnClickListener {

    private FellaFeedback fellaFeedback;
    private FeedbackCallbacks callbacks;

    public FeedbackFragment(FeedbackCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    public FeedbackFragment(FellaFeedback fellaFeedback, FeedbackCallbacks callbacks) {
        this.fellaFeedback = fellaFeedback;
        this.callbacks = callbacks;
    }

    private ScrollView scrollView;
    private LinearLayout container;
    private FeedbackPreferences preferences;
    private UploadingFeedback uploadingFeedback;
    private HashMap<Integer, Answers> answersHashMap;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feedback, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        answersHashMap = new HashMap<>();

        preferences = new FeedbackPreferences(getContext());
        scrollView = view.findViewById(R.id.scrollView);
        container = view.findViewById(R.id.feedback_container);

        uploadingFeedback = new UploadingFeedback();

        FloatingActionButton floatingActionButton = view.findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(this);
        view.findViewById(R.id.feedbackSubmitButton).setOnClickListener(this);

        onSuggestionView(view);
        onReferralView(view);
        onNpsView(view);
        if (fellaFeedback == null)
            onCustomerInformationView(view);
        else
            consumeInput();

        FeedbackForm feedbackForm = FeedbackFormDB.getInstance(getContext()).feedbackFormDao().getFeedbackForm();
        uploadingFeedback.setFeedback_form(Integer.parseInt(feedbackForm.getPk()));
        uploadingFeedback.setFeedbackStart(System.currentTimeMillis() / 1000);

        List<FeedbackQuestions> feedbackQuestions = FeedbackQuestionDB.getInstance(getContext()).feedbackQuestionDao().getQuestionByMapid(Integer.parseInt(feedbackForm.getPk()));
        for (FeedbackQuestions question : feedbackQuestions) {
            String type = question.getType();
            if (type.contentEquals("RATING"))
                createRatingView(question);
            else if (type.contentEquals("YES/NO"))
                createYesNoView(question);
            else if (type.contentEquals("MCQ_NON_RATING") || type.contentEquals("MCQ"))
                createMcqType(question);
            else if (type.contentEquals("TEXT"))
                createTextType(question);
            //TODO
            /*else if (type.contentEquals("DROPDOWN"))
                createDropDownType(question);

            else if (type.contentEquals("CHECKBOXES"))
                createCheckboxesType(question);*/
        }
    }

    private void consumeInput() {
        this.uploadingFeedback.setUserMobile(fellaFeedback.getUserMobile());
        this.uploadingFeedback.setUserFirstName(fellaFeedback.getUserFirstName());
        this.uploadingFeedback.setUserLastName(fellaFeedback.getUserLastName());
        this.uploadingFeedback.setUserEmail(fellaFeedback.getUserEmail());
        this.uploadingFeedback.setUserDob("02/31/1000");
        this.uploadingFeedback.setUserAnni("02/31/1000");
        this.uploadingFeedback.setBill(fellaFeedback.getBillNumber());
        this.uploadingFeedback.setAmount(fellaFeedback.getAmount());
        this.uploadingFeedback.setRemarks(fellaFeedback.getRemarks());
        this.uploadingFeedback.setTable(fellaFeedback.getTableRoomNumber());
    }

    private void createMcqType(final FeedbackQuestions question) {
        createQuestionView(question.getTitle());
        final RecyclerView recyclerView = new RecyclerView(getContext());
        recyclerView.setLayoutParams(
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));

        final List<FeedbackOptions> options = FeedbackOptionDB.getInstance(getContext()).feedbackOptionDao().getOptionsByMapid(question.getPk());
        FeedbackMCQsOption adapter = new FeedbackMCQsOption(getContext(), options);
        GridLayoutManager manager = new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false);
        if (options.size() % 2 != 0) {
            manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (position == options.size() - 1) {
                        return 2;
                    }
                    return 1;
                }
            });
        }

        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        container.addView(recyclerView);

        adapter.setClickListener(new FeedbackMCQsOption.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                List<FeedbackOptions> ans = new ArrayList<>();
                ans.add(options.get(position));
                setAnswers(question.getPk(), String.valueOf(options.get(position).getPk()), options.get(position).getTitle());
                recyclerView.setAdapter(new FeedbackMCQsOption(getContext(), ans));
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
                scroll();
                hideKeyboard(view);
            }
        });
        createDividerView();
    }

    private void createRatingView(final FeedbackQuestions question) {
        createQuestionView(question.getTitle());
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setLayoutParams(
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));


        linearLayout.setLayoutTransition(new LayoutTransition());
        linearLayout.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);

        final int[] img = {preferences.getRateOne(), preferences.getRateTwo(), preferences.getRateThree(), preferences.getRateFour(), preferences.getRateFive()};

        final ImageView imageView = new ImageView(getContext());

        imageView.setLayoutParams(new LinearLayout.LayoutParams(0, 150));
        imageView.setImageResource(img[2]);

        linearLayout.addView(imageView);

        final RatingBar ratingBar = new RatingBar(getContext());
        ratingBar.setNumStars(5);
        ratingBar.setStepSize(1f);
        ratingBar.setLayoutParams(
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));

        linearLayout.addView(ratingBar);

        container.addView(linearLayout);

        final List<FeedbackOptions> options = FeedbackOptionDB.getInstance(getContext()).feedbackOptionDao().getOptionsByMapid(question.getPk());

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if (v > 0) {
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(150, 150));
                    imageView.setImageResource(img[(int) (v - 1)]);
                    ratingBar.setVisibility(View.GONE);
                    int pos = getOptionPosition(String.valueOf((int) v), options);
                    setAnswers(question.getPk(), String.valueOf(options.get(pos).getPk()), String.valueOf((int) v));
                    hideKeyboard(getView());
                    scroll();
                }
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ratingBar.setVisibility(View.VISIBLE);
            }
        });
        createDividerView();
    }

    private void createYesNoView(final FeedbackQuestions question) {
        createQuestionView(question.getTitle());
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setLayoutParams(
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));


        linearLayout.setLayoutTransition(new LayoutTransition());
        linearLayout.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);

        final ImageView noImage = new ImageView(getContext());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(100, 100);
        params.setMargins(16, 16, 16, 16);

        noImage.setLayoutParams(params);
        noImage.setImageResource(R.drawable.ic_no_in);
        linearLayout.addView(noImage);

        final ImageView yesImage = new ImageView(getContext());
        yesImage.setLayoutParams(params);
        yesImage.setImageResource(R.drawable.ic_yes_in);
        linearLayout.addView(yesImage);

        container.addView(linearLayout);

        final boolean[] isAnswered = {false};

        final List<FeedbackOptions> options = FeedbackOptionDB.getInstance(getContext()).feedbackOptionDao().getOptionsByMapid(question.getPk());
        noImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isAnswered[0]) {
                    isAnswered[0] = true;
                    setAnswers(question.getPk(), String.valueOf(options.get(getOptionPosition("no", options)).getPk()), "NO");
                    noImage.setImageResource(R.drawable.ic_no_ac);
                    yesImage.setVisibility(View.GONE);
                } else {
                    noImage.setImageResource(R.drawable.ic_no_in);
                    yesImage.setVisibility(View.VISIBLE);
                    isAnswered[0] = false;
                }
                scroll();
                hideKeyboard(v);
            }
        });

        yesImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isAnswered[0]) {
                    isAnswered[0] = true;
                    setAnswers(question.getPk(), String.valueOf(options.get(getOptionPosition("yes", options)).getPk()), "YES");
                    noImage.setVisibility(View.GONE);
                    yesImage.setImageResource(R.drawable.ic_yes_ac);
                } else {
                    yesImage.setImageResource(R.drawable.ic_yes_in);
                    noImage.setVisibility(View.VISIBLE);
                    isAnswered[0] = false;
                }
                scroll();
                hideKeyboard(v);
            }
        });
        createDividerView();
    }

    private void createTextType(final FeedbackQuestions question) {
        createQuestionView(question.getTitle());
        TextInputEditText textInputEditText = createEditTextView("Type answer here...", InputType.TYPE_CLASS_TEXT);
        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setAnswers(question.getPk(), "0", s.toString());
            }
        });
        createDividerView();
    }

    private TextInputEditText createEditTextView(String hint, int inputType) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(32, 16, 32, 16);
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(getContext()).inflate(R.layout.text_input_edit_text, null);
        TextInputLayout layout = view.findViewById(R.id.singlePageFeedbackTextInputLayout);
        layout.setLayoutParams(params);
        layout.setHint(hint);
        TextInputEditText editText = view.findViewById(R.id.singlePageFeedbackTextInput);
        editText.setInputType(inputType);
        container.addView(view);
        return editText;
    }

    private void createQuestionView(String title) {
        TextView questionView = new TextView(getContext());
        questionView.setGravity(Gravity.CENTER);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(32, 8, 32, 8);
        questionView.setLayoutParams(params);
        questionView.setPadding(8, 4, 8, 4);
        questionView.setTextColor(getResources().getColor(android.R.color.black));
        questionView.setTextSize(20f);
        questionView.setTypeface(null, Typeface.BOLD);
        questionView.setText(title);
        container.addView(questionView);
    }

    private void createDividerView() {
        RelativeLayout relativeLayout = new RelativeLayout(getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);

        params.setMargins(8, 96, 8, 96);
        relativeLayout.setLayoutParams(params);
        relativeLayout.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        container.addView(relativeLayout);
    }

    private int getOptionPosition(String text, List<FeedbackOptions> optionsList) {
        int pos = 0;
        for (int i = 0; i < optionsList.size(); i++) {
            if (optionsList.get(i).getTitle().toLowerCase().contains(text))
                pos = i;
        }
        return pos;
    }

    private void setAnswers(Integer question, String optionPK, String answerText) {
        if (answersHashMap.containsKey(question)) {
            answersHashMap.remove(question);
            answersHashMap.put(question, new Answers(question, optionPK, answerText));
        } else {
            answersHashMap.put(question, new Answers(question, optionPK, answerText));
        }
    }

    private void onSuggestionView(View view) {
        if (!preferences.isSkipSuggestion()) {
            view.findViewById(R.id.suggestionContainer).setVisibility(View.VISIBLE);
            TextView suggestionMessageView = view.findViewById(R.id.suggestionMessage);
            suggestionMessageView.setText(preferences.getSuggestionMessage());
            TextInputEditText suggestionInput = view.findViewById(R.id.suggestionInput);
            suggestionInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    uploadingFeedback.setComment(editable.toString().trim());
                }
            });
        }
    }

    private void onReferralView(View view) {
        if (preferences.isReferralFeature()) {
            view.findViewById(R.id.referralContainer).setVisibility(View.VISIBLE);
            TextView referralMessageView = view.findViewById(R.id.referralMessage);
            referralMessageView.setText(preferences.getReferralMessage());
            TextInputEditText referralInput = view.findViewById(R.id.referralPhoneInput);
            referralInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    uploadingFeedback.setReferto(editable.toString().trim());
                }
            });
        }
    }

    private void onNpsView(View view) {
        if (!preferences.isSkipNPS()) {
            view.findViewById(R.id.npsContainer).setVisibility(View.VISIBLE);
            TextView npsMessageView = view.findViewById(R.id.npsMessage);
            npsMessageView.setText(preferences.getNpsMessage());
            SeekBar npsInput = view.findViewById(R.id.npsInput);
            npsInput.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    uploadingFeedback.setNps(seekBar.getProgress());
                }
            });
        }
    }

    private void setDefaultData() {
        uploadingFeedback.setAmount("");
        uploadingFeedback.setBill("");
        uploadingFeedback.setComment("");
        uploadingFeedback.setRemarks("");
        uploadingFeedback.setTable("");
        uploadingFeedback.setTag("");
        uploadingFeedback.setUserEmail(" ");
        uploadingFeedback.setUserFirstName(" ");
        uploadingFeedback.setUserMobile(" ");
        uploadingFeedback.setUserLastName(" ");
    }

    private void onCustomerInformationView(View view) {
        setDefaultData();
        view.findViewById(R.id.ctaContainer).setVisibility(View.VISIBLE);
        TextView ctaMessageView = view.findViewById(R.id.ctaMessage);
        ctaMessageView.setText(preferences.getCtaMessage());
        TextInputEditText ctaPhoneInput = view.findViewById(R.id.ctaPhoneInput);
        ctaPhoneInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                uploadingFeedback.setUserMobile(editable.toString().trim());
            }
        });

        TextInputEditText ctaNameInput = view.findViewById(R.id.ctaNameInput);
        ctaNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                uploadingFeedback.setUserFirstName(editable.toString().trim());
            }
        });

        TextInputEditText ctaEmailInput = view.findViewById(R.id.ctaEmailInput);
        ctaEmailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                uploadingFeedback.setUserEmail(editable.toString().trim());
            }
        });
    }

    private void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void scroll() {
        scrollView.smoothScrollBy(0, (int) (scrollView.getMaxScrollAmount() * .5));
    }

    @Override
    public void onClick(View view) {
        List<Answers> answersList = new ArrayList<>(answersHashMap.values());
        uploadingFeedback.setAnswers(answersList);
        uploadingFeedback.setTimestamp(System.currentTimeMillis() / 1000);
        new NetworkingCalls(getContext(), new NetworkingCallbacks() {
            @Override
            public void onComplete() {

            }

            @Override
            public void onSuccess() {
                Toast.makeText(getContext(), "Uploaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailed() {
                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
            }
        }).uploadFeedback(uploadingFeedback);
    }
}
