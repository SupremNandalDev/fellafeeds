/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
interface FeedbackOptionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFeedbackQuestion(FeedbackOptions feedbackOptions);

    @Query("DELETE FROM feedback_options")
    void removeFeedbackOptions();

    @Query("SELECT * FROM feedback_options")
    List<FeedbackOptions> getAllOptions();

    @Query("SELECT * FROM feedback_options where mapid =:mapid")
    List<FeedbackOptions> getOptionsByMapid(int mapid);

    @Query("SELECT * FROM feedback_options where pk =:pk")
    FeedbackOptions getOptionsByPk(int pk);

}
