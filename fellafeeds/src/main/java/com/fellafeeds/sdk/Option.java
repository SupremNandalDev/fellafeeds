/*
 * Code Author : Suprem Nandal (suprem.nandal@gmail.com, +919560787007)
 */

package com.fellafeeds.sdk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
class Option {

    @SerializedName("ss")
    @Expose
    private Integer ss;
    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("flow")
    @Expose
    private String flow;
    @SerializedName("end_of_survey")
    @Expose
    private boolean endOfSurvey;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isEndOfSurvey() {
        return endOfSurvey;
    }

    public void setEndOfSurvey(boolean endOfSurvey) {
        this.endOfSurvey = endOfSurvey;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public Integer getSs() {
        return ss;
    }

    public void setSs(Integer ss) {
        this.ss = ss;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}