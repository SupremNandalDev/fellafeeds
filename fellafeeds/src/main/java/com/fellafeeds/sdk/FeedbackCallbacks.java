package com.fellafeeds.sdk;

public interface FeedbackCallbacks {

    void onCancel();

    void onComplete();

    void onSuccess();

    void onUnsuccess();

}
