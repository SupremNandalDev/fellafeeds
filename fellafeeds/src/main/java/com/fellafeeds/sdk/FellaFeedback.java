package com.fellafeeds.sdk;

public class FellaFeedback {

    private String userMobile;
    private String userFirstName;
    private String userLastName;
    private String userEmail;
    private String billNumber;
    private String amount;
    private String remarks;
    private String tableRoomNumber;

    public FellaFeedback() {
    }

    public FellaFeedback(String userMobile, String userFirstName, String userLastName, String userEmail) {
        this.userMobile = userMobile;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userEmail = userEmail;
    }

    public FellaFeedback(String userMobile, String userFirstName, String userLastName, String userEmail, String billNumber, String amount, String remarks, String tableRoomNumber) {
        this.userMobile = userMobile;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userEmail = userEmail;
        this.billNumber = billNumber;
        this.amount = amount;
        this.remarks = remarks;
        this.tableRoomNumber = tableRoomNumber;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTableRoomNumber() {
        return tableRoomNumber;
    }

    public void setTableRoomNumber(String tableRoomNumber) {
        this.tableRoomNumber = tableRoomNumber;
    }
}
