# Implementation:
Project Level Gradle:
```sh
allprojects {
repositories {
       google()
       jcenter()
       maven { url 'https://jitpack.io' }
    }
}
```
[![](https://jitpack.io/v/com.gitlab.SupremNandalDev/fellafeeds.svg)](https://jitpack.io/#com.gitlab.SupremNandalDev/fellafeeds)

App Level Gradle:
```sh
dependencies {
	        implementation 'com.gitlab.SupremNandalDev:fellafeeds:{latest_version}}’
	}
```
# Uses:

Resources Initialization:
```sh
import com.fellafeeds.sdk.FeedbackPreferences;
import com.fellafeeds.sdk.NetworkingCallbacks;
import com.fellafeeds.sdk.NetworkingCalls;
```

```sh
public class WelcomeActivity extends AppCompatActivity implements NetworkingCallbacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
	
	//Authentication -- email : team@fellafeeds.com
    FeedbackPreferences preferences = new FeedbackPreferences(getApplicationContext());

    preferences.setAuthToken("dfgvhb4355x543x43h43h4d3y435d4sdvfd");

    //Download Feedback forms and other Resources
    NetworkingCalls networkingCalls = new NetworkingCalls(getApplicationContext(),this);
    networkingCalls.getFeedbackForms();

    }

    @Override
    public void onComplete() {
        
    }
    
    @Override
    public void onSuccess() {
        //Resources Downloaded Successfully...
    }
    
    @Override
    public void onFailed() {
        //Failed to Download Resources...
    }	
}
```

Starting Feedback Session:

```sh
    import com.fellafeeds.sdk.FeedbackCallbacks;
    import com.fellafeeds.sdk.FeedbackFragment;
    import com.fellafeeds.sdk.FellaFeedback;
```

```sh
	public class FeedbackActivity extends AppCompatActivity implements FeedbackCallbacks {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		
		//Pass User Informations to Fragment
		FellaFeedback fellaFeedback = new FellaFeedback();
        fellaFeedback.setAmount();   //param(String)
        fellaFeedback.setBillNumber(); //param(String)
        fellaFeedback.setRemarks();//param(String)
        fellaFeedback.setTableRoomNumber();//param(String)
        fellaFeedback.setUserEmail();//param(String- valid email address)
        fellaFeedback.setUserFirstName();//param(String)
        fellaFeedback.setUserLastName();//param(String)
        fellaFeedback.setUserMobile();//param(String -with or without country code)
        
        //initialize fragment for feedback session
        FeedbackFragment feedbackFragment = new FeedbackFragment(fellaFeedback,this);
        
        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.feedbackFragmentContainer, 
            feedbackFragment)
            .commit();
    }

    @Override
    public void onCancel() {
        
    }
    
    @Override
    public void onComplete() {
    
    }
    
    @Override
    public void onSuccess() {
        //Feedback Session Completed...
    }
    
    @Override
    public void onUnsuccess() {
        // Feedback Session Failed...
    }
}
```