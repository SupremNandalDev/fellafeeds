package com.fellafeeds.example;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.fellafeeds.sdk.FeedbackCallbacks;
import com.fellafeeds.sdk.FeedbackFragment;
import com.fellafeeds.sdk.FellaFeedback;

public class    FeedbackActivity extends AppCompatActivity implements FeedbackCallbacks{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        FellaFeedback fellaFeedback = new FellaFeedback();
        fellaFeedback.setAmount("69999");
        fellaFeedback.setBillNumber("ddf877w45s7");
        fellaFeedback.setRemarks("no remark");
        fellaFeedback.setTableRoomNumber("q1-150");
        fellaFeedback.setUserEmail("suprem.nandal.123@gmail.com");
        fellaFeedback.setUserFirstName("Suprem");
        fellaFeedback.setUserLastName("Nandal");
        fellaFeedback.setUserMobile("9560787007");

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.feedbackFragmentContainer, new FeedbackFragment(fellaFeedback,FeedbackActivity.this))
                .commit();

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onSuccess() {
        Toast.makeText(getApplicationContext(), "Feedback Uploaded...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUnsuccess() {
        Toast.makeText(getApplicationContext(), "can't upload...", Toast.LENGTH_SHORT).show();
    }
}
