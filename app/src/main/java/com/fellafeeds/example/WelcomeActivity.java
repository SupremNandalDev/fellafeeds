package com.fellafeeds.example;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.fellafeeds.sdk.FeedbackPreferences;
import com.fellafeeds.sdk.NetworkingCallbacks;
import com.fellafeeds.sdk.NetworkingCalls;

public class WelcomeActivity extends AppCompatActivity implements NetworkingCallbacks {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        FeedbackPreferences preferences = new FeedbackPreferences(getApplicationContext());
        preferences.setAuthToken("provided by fellafeeds....");

        progressDialog = new ProgressDialog(WelcomeActivity.this);
        progressDialog.setMessage("Downloading Resources");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        findViewById(R.id.downloadButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                new NetworkingCalls(getApplicationContext(),WelcomeActivity.this).getFeedbackForms();
            }
        });

    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onSuccess() {
        progressDialog.dismiss();
        Toast.makeText(getApplicationContext(), "Resources Downloaded Successfully...", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getApplicationContext(),FeedbackActivity.class));
    }

    @Override
    public void onFailed() {
        progressDialog.dismiss();
        Toast.makeText(getApplicationContext(), "Failed with unknown reason...", Toast.LENGTH_SHORT).show();
    }
}